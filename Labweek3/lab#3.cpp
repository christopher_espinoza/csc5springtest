/* 
 * File:   main.cpp
 * Author: Chris
 *
 * Created on March 5, 2015, 2:29 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    int x;
    int y;
    int temp;
    cout << "Please enter two values less than 1000" << endl;
    cout << "x=";
    cin>> x;
    cout << "y=";
    cin>> y;
    cout << "-------------------------------------------------" << endl;
    
    temp = x;
    x = y;
    y = temp;

    cout<<"x="<<x<<endl;
    cout<<"y=" <<y;

    return 0;
}

