/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 3, 2015, 10:54 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
//Prompt user for two numbers
    cout<< "please enter two numbers:";
    
    int num1, num2;
    cin >> num1 >>num2;
    
    //Use static cast to convert to double 
    double avg = (num1 + num2)/2.0;
    
    cout<< Left
    cout<< setw(10) << "Value 1";
    cout<< setw(10) << "Value 2";
    
    cout<< setw(10) << num1;
    cout<< setw(10) << num2;
    
    cout<< endl;
    
    cout<< setw(10) << "Avg:";
    cout<< setw(10) << avg;
    return 0;
}

